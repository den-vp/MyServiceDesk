﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyServiceDesk.Domain.Entities
{
    public class User: IdentityUser
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длина записи")]
        public string Name { get; set; }        
        [Required]
        [Display(Name = "Фамилия пользователя")]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длина записи")]
        public string SurName { get; set; }
        [Display(Name = "Отчество пользователя")]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длина записи")]
        public string MiddleName { get; set; }
        [Display(Name = "Должность")]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длина записи")]
        public string Position { get; set; }
        [Display(Name = "Отдел")]
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
    }
}
